        <footer class="footer">
            <div class="container">
                <div class="footer-top">
                    <a class="footer-logo" href="<?php echo get_home_url(); ?>">
                        <picture>
                            <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/logo-2@x2.png"><img src="<?=get_template_directory_uri()?>/assets/images/logo-2.png">
                        </picture>
                    </a>
                    <?php 
                        echo wp_unitso_menu( 'Header', [
                            'menu_class' => 'footer-nav',
                            'item_class' => 'footer-nav__elem'
                        ]);
                    ?>
                    <div class="footer-info__list">
                        <div class="footer-info">
                            <div class="footer-info__icon">
                                <svg class="icon icon-pin ">
                                    <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#pin"></use>
                                </svg>
                            </div>
                            <div class="footer-info__title">Koninginnegracht 5, 2514AA The Hague, The&nbsp;Netherlands</div>
                        </div>
                        <div class="footer-info">
                            <div class="footer-info__icon">
                                <svg class="icon icon-phone ">
                                    <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#phone"></use>
                                </svg>
                            </div>
                            <div class="footer-info__title">+31 6 389 003 01</div>
                        </div>
                        <div class="footer-info">
                            <div class="footer-info__icon">
                                <svg class="icon icon-info ">
                                    <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#info"></use>
                                </svg>
                            </div>
                            <div class="footer-info__title">info@unitso.com</div>
                        </div>
                        <div class="footer-info">
                            <div class="footer-info__icon">
                                <svg class="icon icon-clock ">
                                    <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#clock"></use>
                                </svg>
                            </div>
                            <div class="footer-info__title">Mon to Fri - 09:00 to 18:00</div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="footer-text">
                        <p>Chamber of Commerce (KvK) no.: 61629642 <span>VAT number (BTW-id): NL002525228B24</span></p>
                    </div><a class="footer-icon" href="https://www.linkedin.com/company/80731384/" target="_blank">
                        <picture>
                            <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/in@x2.png"><img src="<?=get_template_directory_uri()?>/assets/images/in.png">
                        </picture></a>
                </div>
                <div class="footer-copyright">© <?php echo date("Y"); ?> Unitso. All rights reserved.</div>
            </div>
        </footer>
        <?php
        wp_footer();
        ?>
		<script src="//code.tidio.co/9jsagkvpkvp55i0wgwg9xmwqyg2ipmvc.js" async></script>
    </body>
</html>