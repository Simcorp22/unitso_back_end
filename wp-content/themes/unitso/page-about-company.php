<?php
/*
Template Name: About Page
*/
the_post();
get_header();

$page_meta = get_post_meta(get_the_ID(), '', true);
?>

<div class="page">
        <section class="page-banner">
            <div class="container">
                <h1 class="page-banner__title">About</h1>
                <div class="page-banner__image">
                    <picture>
                        <source media="(max-width: 767px)" srcset="<?=get_template_directory_uri()?>/assets/images/mob-about-banner.png"/>
                        <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/about-banner@x2.png"/><img src="<?=get_template_directory_uri()?>/assets/images/about-banner.png"/>
                    </picture>
                </div>
            </div>
        </section>
        <section class="section section-advantages">
            <div class="container">
                <div class="advantages-top">
                    <p>Our team of SimCorp Dimension specialists provides the regular support and development services in the following Dimension areas:</p>
                </div>
                <div class="advantages-grid__wrap">
                    <div class="advantages-grid">
                        <?php

                        for($i=1;$i<10;$i++){
                            if(!isset($page_meta['card_'.$i.'_title'])){
                                continue;
                            }
                            ?>
                            <div class="advantages-col">
                                <div class="advantages-card">
                                    <div class="advantages-card__icon">
                                        <?php echo $page_meta['card_'.$i.'_icon'][0]?>
                                    </div>
                                    <div class="advantages-card__text">
                                        <p><b><?php echo $page_meta['card_'.$i.'_title'][0]?></b></p>
                                        <p><?php echo $page_meta['card_'.$i.'_text'][0]?></p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-article">
            <div class="container">
                <div class="about-article__flex">
                    <div class="about-article__image"><img src="<?=get_template_directory_uri()?>/assets/images/about2.png">
                        <h3 class="about-article__title"><?php echo $page_meta['prev_name'][0]?></h3>
                        <h4 class="about-article__subtitle"><?php echo $page_meta['prev_position'][0]?></h4>
                        <p><?php echo $page_meta['prev_regards'][0]?></p>
                    </div>
                    <div class="about-article__text">
                        <?php echo $page_meta['prev_description'][0]?>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php get_footer(); ?>
