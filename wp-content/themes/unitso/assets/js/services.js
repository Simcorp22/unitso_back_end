var solutionSlider;

$( document ).ready(function() {

	if ($('.js-solutionSlider').length > 0) {
		var page = getSlideActiveProduct(solutionSlider);
		
		solutionSlider = new Swiper('.js-solutionSlider .swiper-container', {
			speed: 600,
			initialSlide: page,
			// autoHeight: true,
			navigation: {
			  nextEl: '.js-solutionSlider .swiper-next',
			  prevEl: '.js-solutionSlider .swiper-prev'
			}
		});

		

	}

	function getSlideActiveProduct(solutionSlider){
		for(var i=0; i < $('.solution-button').length; i++){
			if($('.solution-button:eq('+i+')').hasClass('is-active'))
			{
				return Math.floor( i / 4 );
			}
		}
	}
    
	$('.solution-tab').click(function(e){
		choseCat(e, this);
	});

	$('.solution-button').click(function(e){
		choseProduct(e, this);
		changeUrlByElement(this);
	});

	changeUrlByElement($('.solution-button.is-active'));

	$('.modal-form').submit(function(e)
	{
		e.preventDefault();

		if($('#modal_submit_btn').prop('disabled'))
		{
			return;
		}
		$('#modal_submit_btn').prop('disabled', true);
		var data = $( this ).serialize();

		var data = {
			action: 'unitso_make_order',
			data: data
		};
		
		$.ajax({
			url: '/wp-admin/admin-ajax.php',
			data: data,
			type: "POST",
			success: function(response) {
				
			   	if(response){
					$('.modal-title').html('Thank you!');
					$('.modal-box .modal-form').remove();
					$('.modal-box').append('<h2>Your order is received.</h2>');
					$(document).click(function(){
						location.reload();
					});
				}
			}
		});
	});
});

function changeUrlByCat(element){
	var tabSlug = $(element).attr('data-slug');
	var slug = $('.solution-button.is-active').attr('data-slug');
	var urlPath = '';
	urlPath = '/solutions-services/'+tabSlug+'/'+slug+'/';

	var title = $(element).text();
	const state = {pageTitle : "title"};

	window.history.pushState(state, title, urlPath);
}

function changeUrlByElement(element){
	var tabSlug = $('.solution-tabs .is-active').attr('data-slug');
	var slug = $(element).attr('data-slug');

	var urlPath = '';
	urlPath = '/solutions-services/'+tabSlug+'/'+slug+'/';
	
	
	var title = $(element).text();
	const state = {pageTitle : "title"};

	window.history.pushState(state, title, urlPath);

}

function choseProduct(event, element)
{
	event.preventDefault();
	var post_id = $(element).attr('data-id');
	var data = {
		action: 'unitso_cat_product',
		post_id: post_id
	};
	
	$('.solution-button').removeClass('is-active');
	$(element).addClass('is-active');
	//$('.selDiv option:eq(1)').prop('selected', true)
	$('.form-select option[value="'+post_id+'"]').prop('selected', true);
	$('.select2-selection__rendered').text($(element).text());
	$( "#loading_modal" ).removeClass( "hidden" );

	$.ajax({
		url: '/wp-admin/admin-ajax.php',
		data: data,
		type: "POST",
		success: function(response) {
			
		   	if(response){
				var data = JSON.parse(response);
				$('.solution-title').text(data.post.post_title);
				$('.solution-text').html(data.post.post_content);
				$('.solution-list').html('');
				$('.solution-list').html(data.solutions);
				$('.dropbox-body').html(data.meta_values.details[0]);
			}
		   	
			$( "#loading_modal" ).addClass( "hidden" );
		}
	});
}

function choseCat(event, element)
{
	event.preventDefault();
	var cat_id = $(element).attr('data-id');
	var data = {
		action: 'unitso_cat_products',
		cat_id: cat_id
	};

	$('.solution-tab').removeClass('is-active');
	$(element).addClass('is-active');
	$( "#loading_modal" ).removeClass( "hidden" );

	$.ajax({
		url: '/wp-admin/admin-ajax.php',
		data: data,
		type: "POST",
		success: function(response) {
			if(response)
			{
				var data = JSON.parse(response);
				$('.swiper-wrapper:eq(0)').html(data.categories);
				solutionSlider.update();
				solutionSlider.slideTo(0, 0);
				$('.solution-button').click(function(e){
					choseProduct(e, this);
					changeUrlByElement(this);
				});

				$('.solution-title').text(data.post.post_title);
				$('.solution-text').html(data.post.post_content);
				$('.solution-list').html('');
				$('.solution-list').html(data.solutions);
				$('.dropbox-body').html(data.meta_values.details[0]);
				$('.form-select').html(data.options);
				$('.select2-selection__rendered').text($('.form-select option:eq(0)').text());

				changeUrlByCat(element);
			}
		   	

			$( "#loading_modal" ).addClass( "hidden" );
		}
	});
}