$( document ).ready(function() {
	$('.contacts-form').submit(function(e)
	{
		e.preventDefault();

		if($('#contact_submit_btn').prop('disabled'))
		{
			return;
		}
		$( "#loading_modal" ).removeClass( "hidden" );
		$('#contact_submit_btn').prop('disabled', true);
		var data = $( this ).serialize();

		var data = {
			action: 'unitso_send_feed',
			data: data
		};
		
		$.ajax({
			url: '/wp-admin/admin-ajax.php',
			data: data,
			type: "POST",
			success: function(response) {
				
			   	if(response){
					$('.modal-title').html('Thank you!');
					$('.modal-box').append('<h2>Your message is received.</h2>');
					$('#modal_form_container').css('display', 'block');
					$(document).click(function(){
						location.reload();
					});
				}
			   	
				$( "#loading_modal" ).addClass( "hidden" );
			}
		});
	});
});