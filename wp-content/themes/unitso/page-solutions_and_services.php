<?php
/*
Template Name: Solutions & Services Page
*/
wp_enqueue_script('unitso-services', get_template_directory_uri() . '/assets/js/services.js', array('jquery', 'unitso-main-scripts', 'unitso-vendor-scripts'), null, true);


function getProductKeyFromURI($products){
	$URI_PARTS = explode( '/', $_SERVER['REQUEST_URI']);
	if(empty($URI_PARTS[3]))
	{
		if(empty($URI_PARTS[2]))
		{
			return 0;
		}
		$prodName = $URI_PARTS[2];
	}else{
		$prodName = $URI_PARTS[3];
	}
	
	foreach($products as $key => $product){
		
		if($product->post_name == $prodName)
		{
			return $key;
		}
	}

	return 0;
}

function getCatKeyFromURI($cats){
	$URI_PARTS = explode( '/', $_SERVER['REQUEST_URI']);
	if(empty($URI_PARTS[2]))
	{
		return 0;
	}

	foreach($cats as $key => $cat){

		if($cat->slug == $URI_PARTS[2])
		{
			return $key;
		}
	}

	return 0;
}

?>
<?php get_header(); ?>
	<div id="loading_modal" class="hidden">
		<img src="<?=get_template_directory_uri()?>/assets/images/ajax-loading-icon.jpg" alt="">
	</div>
	<div class="page">
		<section class="page-banner">
			<div class="container">
				<h1 class="page-banner__title size_small">Solutions &amp; Services</h1>
				<div class="page-banner__image style_solution"><img src="<?=get_template_directory_uri()?>/assets/images/solution-banner.png" alt=""></div>
			</div>
		</section>
		<section class="section-solution">
			<div class="container">
				<?php

				$terms = get_terms( [
					'taxonomy' => 'product_category',
					'hide_empty' => false,
					'orderby'       => 'id', 
					'order'         => 'ASC',
				] );

				

				if(empty($terms->errors))
				{
					?>
					<div class="solution-tabs">
						<?php
						$catKey = getCatKeyFromURI($terms);
						//$catKey = 0;
						foreach( $terms as $key => $term ){
							?>
								<div data-slug="<?=$term->slug?>" class="solution-tab unselectable <?=$key==$catKey? 'is-active' : ''?>" data-id="<?=$term->term_taxonomy_id?>"><?=$term->name?></div>
							<?
						}
						?>
					</div>
					<?php

				}
				?>
				<div class="solution-buttons">
					<div class="solution-slider js-solutionSlider">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<?php
								
								$pages = get_posts(array(
									'post_type' => 'product',
									'numberposts' => -1,
									'orderby'     => 'meta_value',
									'order'       => 'ASC',
									'meta_query' => array(
		                                array(
		                                	'key' => 'order',
		                                	'type' => 'NUMERIC'
		                                )
		                            ),
									'tax_query' => array(
										array(
											'taxonomy' => 'product_category',
											'field' => 'term_id', 
											'terms' => $terms[$catKey]->term_id, /// Where term_id of Term 1 is "1".
											'include_children' => false
										)
									)
								));

								$prodKey = getProductKeyFromURI($pages);

								$maxPostInSlide = 4;
								foreach ($pages as $key => $page) {
									if($key % 4 == 0 || $key == 0) {
										if($key % 4 == 0 && $key != 0)
										{
											?>
												</div>
											<?php
										}
										?>
											<div class="swiper-slide">
										<?php
									}
									
									?>
										<div data-slug="<?=$page->post_name?>" class="solution-button <?=$key==$prodKey? 'is-active' : ''?>" data-id="<?=$page->ID?>"><?=$page->post_title?></div>
									<?php
								}
								?>
								</div>
							</div>
						</div>
						<div class="reviews-slider__nav">
							<div class="swiper-prev">
								<svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M1 4.75H20.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
									<path d="M1 4.75L4 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
									<path d="M1 4.75L4 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
								</svg>
							</div>
							<div class="swiper-next">
								<svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M21 4.75H1.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
									<path d="M21 4.75L18 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
									<path d="M21 4.75L18 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
								</svg>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section-solution">
			<div class="container">
				<div class="solution-about">
					<h2 class="solution-title"><?=$pages[$prodKey]->post_title?></h2>
					<div class="solution-text">
						<?
						//var_dump($pages[1]);
						?>
						<?=$pages[$prodKey]->post_content?>
					</div>
					<?php
					$meta_values = get_post_meta( $pages[$prodKey]->ID, '', true );
					//var_dump($meta_values);
					?>
					<div class="solution-container">
						<div class="solution-list">
							<div class="solution-elem">
								<div class="solution-elem__head">
									<div class="solution-elem__icon">
										<svg class="icon icon-fast ">
											<use xlink:href="<?=$meta_values['specification_icon'][0]?>"></use>
										</svg>
									</div>
									<h3 class="solution-elem__title"><?=$meta_values['specification_name'][0]?></h3>
								</div>
								<div class="solution-elem__body">
									<p><?=$meta_values['specification_description'][0]?></p>
								</div>
							</div>
							<div class="solution-elem">
								<div class="solution-elem__head">
									<div class="solution-elem__icon">
										<svg class="icon icon-scaleble ">
											<use xlink:href="<?=$meta_values['specification_icon_2'][0]?>"></use>
										</svg>
									</div>
									<h3 class="solution-elem__title"><?=$meta_values['specification_name_2'][0]?></h3>
								</div>
								<div class="solution-elem__body">
									<p><?=$meta_values['specification_description_2'][0]?></p>
								</div>
							</div>
							<div class="solution-elem">
								<div class="solution-elem__head">
									<div class="solution-elem__icon">
										<svg class="icon icon-quality ">
											<use xlink:href="<?=$meta_values['specification_icon_3'][0]?>"></use>
										</svg>
									</div>
									<h3 class="solution-elem__title"><?=$meta_values['specification_name_3'][0]?></h3>
								</div>
								<div class="solution-elem__body">
									<p><?=$meta_values['specification_description_3'][0]?></p>
								</div>
							</div>
						</div>
						<?php
						if(strpos($meta_values['details'][0], '<ul')===false)
				        {
				        	$meta_values['details'][0] = trim($meta_values['details'][0]);
				            $data = explode(PHP_EOL, $meta_values['details'][0]);
				            $meta_values['details'][0] = '<ul class="ul">';
				            foreach ($data as $substring) {
				                $meta_values['details'][0] .= '<li>'.$substring.'</li>';
				            }
				            $meta_values['details'][0] .= '</ul>';
				        }

						?>
						<div class="solution-dropbox">
							<div class="dropbox-body js-dropBoxBody" style="display: none">
								<?=$meta_values['details'][0]?>
							</div>
						</div>
						<div class="solution-bottom">
							<div class="solution-buttons__flex"><a class="button style_border js-toggleDropBox" href="javascript:;"><span class="button__text">Details
										<div class="button__icon">
											<svg class="icon icon-arrow-top ">
												<use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#arrow-top"></use>
											</svg>
										</div></span></a><a class="button js-modalLink" href="javascript:;" data-mfp-src="#modalForm"><span class="button__text">Order</span></a></div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="modal modal-form mfp-hide" id="modalForm">
		<div class="modal-box"><a class="modal-close js-modalClose" href="javascript:;">
				<svg class="icon icon-close ">
					<use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#close"></use>
				</svg></a>
			<div class="modal-head">
				<h3 class="modal-title">Make an order</h3>
			</div>
			<form class="modal-form js-validate">
				<div class="modal-form__flex">
					<div class="modal-form__left">
						<div class="form-field">
							<input class="input" type="text" name="name" placeholder="Your name..." required>
						</div>
						<div class="form-field">
							<input class="input" type="email" name="email" placeholder="Your e-mail..." required>
						</div>
						<div class="form-field">
							<input class="input" type="text" name="phone" placeholder="Your phone number..." required>
						</div>
					</div>
					<div class="modal-form__right">
						<div class="form-field">
							<textarea class="textarea" placeholder="Your message..." name="message" required></textarea>
						</div>
					</div>
				</div>
				<div class="form-field">
					<select class="form-select js-select2Single" data-placeholder="Choose product..." name="product" required>
						<?php
						$termsArray = array();
						foreach ($terms as $term) {
							$termsArray[] = $term->term_id;
						}
						$pages = get_posts(array(
							'post_type' => 'product',
							'numberposts' => -1,
							'orderby'     => 'meta_value',
							'order'       => 'ASC',
							'meta_query' => array(
                                array(
                                	'key' => 'order',
                                	'type' => 'NUMERIC'
                                )
                            ),
							'tax_query' => array(
								array(
									'taxonomy' => 'product_category',
									'field' => 'term_id', 
									'terms' => $termsArray,
									'include_children' => false
								)
							)
						));

						foreach ($pages as $key => $page) {
							?>
								<option <?=$key==0? 'selected' : ''?> value="<?=$page->ID?>"><?=$page->post_title?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="modal-form__submit">
					<button id="modal_submit_btn" class="button" type="submit" disabled><span class="button__text">Send</span></button>
				</div>
			</form>
		</div>
	</div>

<?php get_footer(); ?>