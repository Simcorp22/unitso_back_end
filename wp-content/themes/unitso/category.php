<?php
/*
Template Name: Category Page
*/
?>
<?php get_header(); ?>
<?php the_post();?>

    <div class="page">
        <section class="page-banner">
            <div class="container">
                <h1 class="page-banner__title mob_margin_top">News</h1>
                <div class="page-banner__image image_bird">
                    <picture>
                        <source media="(max-width: 767px)" srcset="<?=get_template_directory_uri()?>/assets/images/mob-bird.png"/>
                        <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/bird@x2.png"/><img src="<?=get_template_directory_uri()?>/assets/images/bird.png"/>
                    </picture>
                </div>
            </div>
        </section>
        <section class="news">
            <div class="container">
                <?php
                $current_page = get_queried_object();
                $category     = get_the_category();

                $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                $query = new WP_Query( 
                    array(
                        'paged'         => $paged, 
                        'category_name' => $category[0]->name,
                        'order'         => 'desc',
                        'post_type'     => 'post',
                        'post_status'   => 'publish',
                    )
                );

                if ($query->have_posts()) {
                        while ($query->have_posts()) { 
                        $query->the_post(); 
                        $post_id = get_the_ID();
                        if( has_post_video( $post_id ) )
                        {
                            $post_thumbnail_url = get_the_post_video_image_url( $post_id );
                        }else{
                            $post_thumbnail_url = get_the_post_thumbnail_url( $post_id, array(300, 190) );
                        }
                        ?>

                       <a id="post-<?php the_ID(); ?>" class="news-elem" href="<?php the_permalink() ?>">
                            <?php
                            if($post_thumbnail_url)
                            {
                                ?>
                                <div class="news-elem__image <?=has_post_video( $post_id )? 'style_video' : '';?>">
                                    <picture>
                                        <source media="(max-width: 767px)" srcset="<?=get_the_post_thumbnail_url( $post_id, array(300, 190) )?>"/>
                                        <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=$post_thumbnail_url?>"/><img src="<?=$post_thumbnail_url?>"/>
                                    </picture>
                                    <?php
                                    if( has_post_video( $post_id ) )
                                    {
                                        ?> 
                                        <div class="play">
                                            <svg class="icon icon-play ">
                                                <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#play"></use>
                                            </svg>
                                        </div>

                                        <?php
                                    }

                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="news-elem__content">
                                <h3 class="news-elem__title"><?php the_title(); ?></h3>
                                <div class="date"><?php the_time('j') ?> of <?php the_time('F') ?> <?php the_time('Y') ?></div>
                                <div class="news-elem__text">
                                    <p><?=the_excerpt_max_charlength(300);?></p>
                                </div>
                                <div class="news-elem__link">
                                    <div class="link has_icon">
                                        <div class="link-text">see more</div>
                                        <div class="link-icon">
                                            <svg class="icon icon-arrow-link ">
                                                <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#arrow-link"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <?php
                    }

                    if($query->max_num_pages>1)
                    {
                        $prev = get_previous_posts_page_link();
                        $next = get_next_posts_page_link($query->max_num_pages);
                        
                        ?>
                        <div class="page-pagination"><a class="swiper-prev <?=$prev && $paged>1? '' : 'disabled' ?>" href="<?=$prev?>">
                            <svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 4.75H20.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M1 4.75L4 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M1 4.75L4 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg></a><a class="swiper-next <?=$next? '' : 'disabled' ?>" href="<?=$next?>">
                            <svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M21 4.75H1.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M21 4.75L18 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M21 4.75L18 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg></a>
                        </div>
                        <?php
                    }
                }else{
                    ?>
                    <p>Sorry, no posts matched your criteria.</p>
                    <?php
                }
                ?>

                
            </div>
        </section>
    </div>


<?php get_footer(); ?>

