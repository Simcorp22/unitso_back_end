<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php bloginfo('name'); ?></title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        
        <?php
        wp_head();
        ?>
    </head>
    <body>
        <header class="header">
            <div class="container">
                <div class="header-content">
                    <a class="header-logo" href="<?php echo get_home_url(); ?>">
                        <picture>
                            <source media="(max-width: 767px)" srcset="<?=get_template_directory_uri()?>/assets/images/logo@x2.png"/>
                            <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/logo@x2.png"/><img src="<?=get_template_directory_uri()?>/assets/images/logo.png"/>
                        </picture>
                    </a>

                    <?php 
                        echo wp_unitso_menu( 'Header', [
                            'menu_class' => 'header-nav',
                            'item_class' => 'header-nav__elem'
                        ]);
                    ?>
                    
                    <div class="header-burger">
                        <div class="burger js-openMenu"><i></i><i></i><i></i></div>
                    </div>
                </div>
            </div>
            <div class="menu js-menu">
                <div class="menu-nav js-menuContent">
                    <div class="menu-nav__elem">
                        <a href="<?php echo get_home_url(); ?>">
                            <picture>
                                <source media="(max-width: 767px)" srcset="<?=get_template_directory_uri()?>/assets/images/logo@x2.png"/>
                                <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/logo@x2.png"/><img src="<?=get_template_directory_uri()?>/assets/images/logo.png"/>
                            </picture>
                        </a>
                    </div>
                    <?php 
                        echo wp_unitso_menu( 'Header', [
                            'menu_class' => false,
                            'item_class' => 'menu-nav__elem'
                        ]);
                    ?>
                </div>
            </div>
        </header>
        <div class="page-bg"></div>