<?php
 

function wpb_sender_name( $original_email_from ) {
    return get_bloginfo('name');
}

add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

add_action('wp_enqueue_scripts', 'unitso_styles');
add_action('wp_enqueue_scripts', 'unitso_scripts');

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

function unitso_styles(){
	wp_enqueue_style('unitso-style-main' ,get_template_directory_uri() . '/assets/css/main.css');
};
function unitso_scripts(){
	wp_enqueue_script('unitso-vendor-scripts', get_template_directory_uri() . '/assets/js/vendor.js', array('jquery'), null, true);
	wp_enqueue_script('unitso-main-scripts', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), null, true);
};

function unit_pre_get_posts( $query ) {  
if( $query->is_main_query() && !$query->is_feed() && !is_admin() && is_category()) {  
    $query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) );  }  } 

add_action('pre_get_posts','unit_pre_get_posts'); 

function unit_request($query_string ) { 
     if( isset( $query_string['page'] ) ) { 
         if( ''!=$query_string['page'] ) { 
             if( isset( $query_string['name'] ) ) { unset( $query_string['name'] ); } } } return $query_string; } 

add_filter('request', 'unit_request');


add_action( 'template_redirect', 'unit_wpse91590' );
function unit_wpse91590() {

    global $wp_query;

    if(!empty($wp_query->query['name']))
    {
        $args = array(
            'post_type'         => 'product',
            'name'              => $wp_query->query['name'],
            'numberposts'    => 1,
            'fields'            => 'ids',
            'orderby'           => array( 'post_date' => 'DESC' ),
        );

        $args['category'] = $wp_query->query['category_name'];

        $posts = get_posts( $args );

        // If our array isn't empty, redirect
        if( ! empty( $posts ) ) {
            status_header( 200 );
            add_action('wp_head', 'unit_serv_head');
            include( get_template_directory() . '/page-solutions_and_services.php' );
            exit;
        }
    }

}

function unit_serv_head(){
    ?>
        <meta property="og:image" content="https://<?php echo $_SERVER['SERVER_NAME']?>/wp-content/uploads/2021/04/Episode-7-EY-what-happens-with-the-assets-around-the-world-today-Part-2Conclusions..jpeg">
        <meta property="og:image:secure_url" content="https://<?php echo $_SERVER['SERVER_NAME']?>/wp-content/uploads/2021/04/Episode-7-EY-what-happens-with-the-assets-around-the-world-today-Part-2Conclusions..jpeg">
    <?php
};



function the_excerpt_max_charlength( $charlength ){
    $excerpt = get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
            echo mb_substr( $subex, 0, $excut );
        } else {
            echo $subex;
        }
        echo '[...]';
    } else {
        echo $excerpt;
    }
}

add_action( 'save_post', 'update_products_order' );
function update_products_order( $post_ID ){

    if ( $_POST['post_type'] != 'product' && $_POST['post_type'] != 'main_comments' && $_POST['post_type'] != 'logos')
        return;

    $pages = get_posts(array(
        'post_type' => $_POST['post_type'],
        'numberposts' => -1,
        'exclude' => array($post_ID),
        'orderby'     => 'meta_value',
        'order'       => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'order',
                'type' => 'NUMERIC'
            )
        )
    ));

    $post_order = get_post_meta( $post_ID, 'order', true );
    $firstPostUpdated = false;

    $updated_order = 1;
    foreach ($pages as $key => $page) {
       
        if($updated_order < $post_order)
        {
            continue;

        }else if($updated_order == $post_order)
        {
            $updated_order+=1;
        }

        update_post_meta( $page->ID, 'order', $updated_order );

        $updated_order++;
    }
}

function wp_unitso_menu($menu, $attr = array() )
{
    $items = wp_get_nav_menu_items( $menu, array() );

    $htmlItems = '';
    if($attr['menu_class'] != false){
    	$htmlItems .= '<div class="'.$attr['menu_class'].'">';
    }
    
    foreach ($items as $item) {
    	$htmlItems .= '<div class="'.$attr['item_class'].'"><a href="'.$item->url.'">'.$item->title.'</a></div>';
    }
    if($attr['menu_class'] != false){
    	$htmlItems .= '</div>';
    }

    return $htmlItems;
}

function fall_back_menu(){
    return;
}

add_action( 'init', 'true_jquery_register' );

function true_jquery_register() {
	if ( !is_admin() ) {
		wp_deregister_script( 'jquery' );

		wp_register_script( 'jquery', ( get_template_directory_uri() . '/assets/js/jquery-3.5.1.min.js' ), false, null, true );
		
		wp_enqueue_script( 'jquery' );
	}
}

add_action( 'wp_ajax_unitso_send_feed', 'unitso_send_feed' );
add_action( 'wp_ajax_nopriv_unitso_send_feed', 'unitso_send_feed' );

function unitso_send_feed()
{
    $data = array();
    parse_str($_POST['data'], $data);

    $title = wp_strip_all_tags( $data['email'].', name:'.$data['name'] );
    $content = 'Name: '.$data['name'].' <br>
    Email: '.$data['email'].' <br>
    Phone number: '.$data['phone'].' <br>
    Message: '.$data['message'].'
    ';

    $message = array(
      'post_title'    => $title,
      'post_content'  => $content,
      'post_type'     => 'contact_form',
      'post_status'   => 'publish',
    );

    $post_id = wp_insert_post( $message );
    if(!is_wp_error($post_id)){
        $admin_email = get_option( 'admin_email' );
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $admin_email, 'New message from '.$data['email'], $content, $headers, array() );

        $content = "Dear customer <br><br>
            Your message is received successfully. We will analyze it, and will contact you as soon as possible. <br><br><br>

            Best regards, <br><br>
            Unitso team";

        $headers[] = 'From: <info@unitso.com>';
        wp_mail( $data['email'], 'Your message is received successfully!', $content, $headers, array() );

        echo 'success';
    }else{
      echo $post_id->get_error_message();
    }
    

    die();
}

add_action( 'wp_ajax_unitso_make_order', 'unitso_make_order' );
add_action( 'wp_ajax_nopriv_unitso_make_order', 'unitso_make_order' );

function unitso_make_order()
{
    $data = array();
    parse_str($_POST['data'], $data);

    $title = wp_strip_all_tags( $data['email'].', product id:'.$data['product'] );
    $content = 'Name: '.$data['name'].' <br>
    Email: '.$data['email'].' <br>
    Phone number: '.$data['phone'].' <br>
    Product id: '.$data['product'].' <br>
    Product url: '.get_site_url( null, '/wp-admin/post.php?post='.$data['product'].'&action=edit', null ).' <br>
    Message: '.$data['message'].'
    ';

    $order = array(
      'post_title'    => $title,
      'post_content'  => $content,
      'post_type'     => 'orders',
      'post_status'   => 'publish',
    );

    $post_id = wp_insert_post( $order );
    if(!is_wp_error($post_id)){
        $admin_email = get_option( 'admin_email' );
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $admin_email, 'New order from '.$data['email'], $content, $headers, array() );

        $content = "Dear customer <br><br>
            Your order is received successfully. We will analyze it, and will contact you as soon as possible. <br><br><br>

            Best regards, </br><br>
            Unitso team";

        $headers[] = 'From: <info@unitso.com>';
        wp_mail( $data['email'], 'Your order is received successfully!', $content, $headers, array() );

        echo 'success';
    }else{
      echo $post_id->get_error_message();
    }

    die();
}

add_action( 'wp_ajax_unitso_cat_products', 'unitso_get_cat_products' );
add_action( 'wp_ajax_nopriv_unitso_cat_products', 'unitso_get_cat_products' );
function unitso_get_cat_products() {
    $cat_id = intval( $_POST['cat_id'] );

    $pages = get_posts(array(
        'post_type' => 'product',
        'numberposts' => -1,
        'orderby'     => 'meta_value',
        'order'       => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'order',
                'type' => 'NUMERIC'
            )
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'product_category',
                'field' => 'term_id', 
                'terms' => $cat_id,
                'include_children' => false
            )
        )
    ));
    $response = array();
    if($pages)
    {
        ob_start();
        $maxPostInSlide = 4;
        foreach ($pages as $key => $page) {
            if($key % 4 == 0 || $key == 0) {
                if($key % 4 == 0 && $key != 0)
                {
                    ?>
                        </div>
                    <?php
                }
                ?>
                    <div class="swiper-slide">
                <?php
            }
            
            ?>
                <div data-slug="<?=$page->post_name?>" class="solution-button <?=$key==0? 'is-active' : ''?>" data-id="<?=$page->ID?>"><?=$page->post_title?></div>
            <?php
        }
        ?>
        </div>
        <?php
        $response['categories'] = ob_get_contents();
        ob_end_clean();

        $terms = get_terms( [
            'taxonomy' => 'product_category',
            'hide_empty' => false,
            'orderby'       => 'id', 
            'order'         => 'ASC',
        ] );
        $termsArray = array();
        foreach ($terms as $term) {
            $termsArray[] = $term->term_id;
        }
        $pages = get_posts(array(
            'post_type' => 'product',
            'numberposts' => -1,
            'orderby'     => 'meta_value',
            'order'       => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'order',
                    'type' => 'NUMERIC'
                )
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_category',
                    'field' => 'term_id', 
                    'terms' => $termsArray,
                    'include_children' => false
                )
            )
        ));

        ob_start();
        foreach ($pages as $key => $page) {
           

            ?>
                <option <?=$key==0? 'selected' : ''?> value="<?=$page->ID?>"><?=$page->post_title?></option>
            <?php
        }

        $response['options'] = ob_get_contents();
        ob_end_clean();

        $post_id = $pages[0]->ID;
        $response['post'] = wp_get_single_post( $post_id, 'ARRAY_A' );
        if($response['post'])
        {
            $response['meta_values'] = get_post_meta( $response['post']['ID'], '', true );
            if(strpos($response['meta_values']['details'][0], '<ul')===false)
            {
                $response['meta_values']['details'][0] = trim($response['meta_values']['details'][0]);
                $data = explode(PHP_EOL, $response['meta_values']['details'][0]);
                $response['meta_values']['details'][0] = '<ul class="ul">';
                foreach ($data as $substring) {
                    $response['meta_values']['details'][0] .= '<li>'.$substring.'</li>';
                }
                $response['meta_values']['details'][0] .= '</ul>';
            }
            ob_start();
            ?>
                <div class="solution-elem">
                    <div class="solution-elem__head">
                        <div class="solution-elem__icon">
                            <svg class="icon icon-fast ">
                                <use xlink:href="<?=$response['meta_values']['specification_icon'][0]?>"></use>
                            </svg>
                        </div>
                        <h3 class="solution-elem__title"><?=$response['meta_values']['specification_name'][0]?></h3>
                    </div>
                    <div class="solution-elem__body">
                        <p><?=$response['meta_values']['specification_description'][0]?></p>
                    </div>
                </div>
                <div class="solution-elem">
                    <div class="solution-elem__head">
                        <div class="solution-elem__icon">
                            <svg class="icon icon-scaleble ">
                                <use xlink:href="<?=$response['meta_values']['specification_icon_2'][0]?>"></use>
                            </svg>
                        </div>
                        <h3 class="solution-elem__title"><?=$response['meta_values']['specification_name_2'][0]?></h3>
                    </div>
                    <div class="solution-elem__body">
                        <p><?=$response['meta_values']['specification_description_2'][0]?></p>
                    </div>
                </div>
                <div class="solution-elem">
                    <div class="solution-elem__head">
                        <div class="solution-elem__icon">
                            <svg class="icon icon-quality ">
                                <use xlink:href="<?=$response['meta_values']['specification_icon_3'][0]?>"></use>
                            </svg>
                        </div>
                        <h3 class="solution-elem__title"><?=$response['meta_values']['specification_name_3'][0]?></h3>
                    </div>
                    <div class="solution-elem__body">
                        <p><?=$response['meta_values']['specification_description_3'][0]?></p>
                    </div>
                </div>
            <?php
            $response['solutions'] = ob_get_contents();
            ob_end_clean();
        }
    }

    echo json_encode($response);

    wp_die();
}

add_action( 'wp_ajax_unitso_cat_product', 'unitso_get_cat_product' );
add_action( 'wp_ajax_nopriv_unitso_cat_product', 'unitso_get_cat_product' );

function unitso_get_cat_product() {
    $response = array();

    $post_id = intval( $_POST['post_id'] );
    $response['post'] = wp_get_single_post( $post_id, 'ARRAY_A' );
    if($response['post'])
    {
        $response['meta_values'] = get_post_meta( $response['post']['ID'], '', true );
        if(strpos($response['meta_values']['details'][0], '<ul')===false)
        {
            $response['meta_values']['details'][0] = trim($response['meta_values']['details'][0]);
            $data = explode(PHP_EOL, $response['meta_values']['details'][0]);
            $response['meta_values']['details'][0] = '<ul class="ul">';
            foreach ($data as $substring) {
                $response['meta_values']['details'][0] .= '<li>'.$substring.'</li>';
            }
            $response['meta_values']['details'][0] .= '</ul>';
        }
        ob_start();
        ?>
            <div class="solution-elem">
                <div class="solution-elem__head">
                    <div class="solution-elem__icon">
                        <svg class="icon icon-fast ">
                            <use xlink:href="<?=$response['meta_values']['specification_icon'][0]?>"></use>
                        </svg>
                    </div>
                    <h3 class="solution-elem__title"><?=$response['meta_values']['specification_name'][0]?></h3>
                </div>
                <div class="solution-elem__body">
                    <p><?=$response['meta_values']['specification_description'][0]?></p>
                </div>
            </div>
            <div class="solution-elem">
                <div class="solution-elem__head">
                    <div class="solution-elem__icon">
                        <svg class="icon icon-scaleble ">
                            <use xlink:href="<?=$response['meta_values']['specification_icon_2'][0]?>"></use>
                        </svg>
                    </div>
                    <h3 class="solution-elem__title"><?=$response['meta_values']['specification_name_2'][0]?></h3>
                </div>
                <div class="solution-elem__body">
                    <p><?=$response['meta_values']['specification_description_2'][0]?></p>
                </div>
            </div>
            <div class="solution-elem">
                <div class="solution-elem__head">
                    <div class="solution-elem__icon">
                        <svg class="icon icon-quality ">
                            <use xlink:href="<?=$response['meta_values']['specification_icon_3'][0]?>"></use>
                        </svg>
                    </div>
                    <h3 class="solution-elem__title"><?=$response['meta_values']['specification_name_3'][0]?></h3>
                </div>
                <div class="solution-elem__body">
                    <p><?=$response['meta_values']['specification_description_3'][0]?></p>
                </div>
            </div>
        <?php
        $response['solutions'] = ob_get_contents();
        ob_end_clean();
    }
    

    echo json_encode($response);

    wp_die();
}

?>