<?php 


get_header();
?>
<section id="content"style="margin-bottom: 0px;">
    
    <div class="section nobg nopadding" style="margin-top: unset; margin-bottom: unset;">
        <div style="background-color: rgba(0, 0, 0, 0); padding-top: 140px; padding-bottom: 220px;">
            <div class="container">
                <div class="text-muted bottommargin-sm">
                    <div class="kama_breadcrumbs">
                    
                    </div>
                </div>
                <div class="heading-block" style="text-align:center;"><h1>Error 404: Page not found</h1></div>
                <div id="posts" class="small-thumbs">

                    
                    
                
                </div>
                
            </div>
        </div>
    </div>
    <!-- / block_114264 -->
</section>
<?php
get_footer();