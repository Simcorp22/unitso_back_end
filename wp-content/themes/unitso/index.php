<?php 
get_header();
the_post();
?>
<section id="content"style="margin-bottom: 0px;">
    
    <div class="section nobg nopadding" style="margin-top: unset; margin-bottom: unset;">
        <div style="background-color: rgba(0, 0, 0, 0); padding-top: 140px; padding-bottom: 60px;">
            <div class="container">
                <div class="text-muted bottommargin-sm">
                    <div class="kama_breadcrumbs">
                    <?php if(function_exists('bcn_display_list')){ bcn_display_list(); } ?>
                    </div>
                </div>
                <div class="heading-block"><h1><?php the_title(); ?></h1></div>
                <div id="posts" class="small-thumbs">

            <?php the_content(); ?>
                    
                
                </div>
                
            </div>
        </div>
    </div>
    <!-- / block_114264 -->
</section>
<?php
get_footer();