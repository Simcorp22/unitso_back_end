<?php get_header(); ?>
<?php

?>
    
    <div class="page">
        <section class="section description">
            <div class="container">
                <div class="description-wrap">
                    <div class="description-content">
                        <h1 class="description-title">We automate & support your SimCorp Dimension environment today</h1>
                        <div class="description-text">
                            <p>so that you stay ahead in the competitive & complex world tomorrow</p>
                        </div>
                    </div>
                    <div class="description-image">
                        <picture>
                            <source media="(max-width: 767px)" srcset="<?=get_template_directory_uri()?>/assets/images/mob-rocket.png"/>
                            <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/rocket@x2.png"/><img src="<?=get_template_directory_uri()?>/assets/images/rocket.png"/>
                        </picture>
                    </div>
                </div>
            </div>
        </section>
        <section class="section reviews">
            <div class="container">
                <div class="reviews-head">
                    <h2 class="page-title">What our customers say:</h2>
                </div>
                <?php
                $logos = get_posts(array(
                    'post_type' => 'logos',
                    'numberposts' => -1,
                    'orderby'     => 'meta_value',
                    'order'       => 'ASC',
                    'meta_query' => array(
                        array(
                            'key' => 'order',
                            'type' => 'NUMERIC'
                        )
                    )
                ));

                
                if(count($logos)>0)
                {
                ?>
                    <div class="logo-slider js-logoSlider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php
                                foreach ($logos as $key => $logo) {
                                    $post_thumbnail_url = get_the_post_thumbnail_url( $logo->ID );
                                    $meta_values = get_post_meta( $logo->ID, '', true );
                                    if($post_thumbnail_url == '')
                                    {
                                        continue;
                                    }
                                    ?>
                                    <div class="reviews-col swiper-slide">
                                        <a class="reviews-image">
                                            <picture>
                                                <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=$post_thumbnail_url?>">
                                                <img src="<?=$post_thumbnail_url?>" <?=$meta_values['width'][0]? 'width="'.$meta_values['width'][0].'"' : '' ?> <?=$meta_values['height'][0]? 'height="'.$meta_values['height'][0].'"' : '' ?>>
                                            </picture>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="reviews-slider__nav">
                            <div class="swiper-prev">
                                <svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 4.75H20.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M1 4.75L4 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M1 4.75L4 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                            </div>
                            <div class="swiper-next">
                                <svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21 4.75H1.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M21 4.75L18 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M21 4.75L18 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="reviews-slider js-reviewsSlider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php
                                
                            $comments = get_posts(array(
                                'post_type' => 'main_comments',
                                'numberposts' => -1,
                                'orderby'     => 'meta_value',
                                'order'       => 'ASC',
                                'meta_query' => array(
                                    array(
                                        'key' => 'order',
                                        'type' => 'NUMERIC'
                                    )
                                )
                            ));

                            foreach ($comments as $key => $comment) {
                                $meta_values = get_post_meta( $comment->ID, '', true );
                                if($meta_values['author_photo'][0] != '')
                                {
                                    $meta_values['author_photo_src'] = wp_get_attachment_image_url( $meta_values['author_photo'][0] );
                                }else{
                                    $meta_values['author_photo_src'] = get_template_directory_uri().'/assets/images/review-14.png';
                                }
                                ?>
                                    <div class="swiper-slide">
                                        <div class="card">
                                            <div class="card-border">
                                                <div class="pattern_1">
                                                    <svg class="icon icon-pattern-1 ">
                                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#pattern-1"></use>
                                                    </svg>
                                                </div>
                                                <div class="pattern_2">
                                                    <svg class="icon icon-pattern-2 ">
                                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#pattern-2"></use>
                                                    </svg>
                                                </div>
                                                <div class="card-border__text">
                                                    <?=$comment->post_content?>
                                                </div>
                                                <?php
                                                if(strlen($comment->post_content) > 276)
                                                {
                                                    ?>
                                                    <div class="card-link__wrap"><a class="card-link js-modalLink" href="javascript:;" data-mfp-src="#modalCard">Read more</a></div>
                                                    <?php
                                                }
                                                ?>
                                                
                                            </div>
                                            <div class="card-body">
                                                <div class="card-icon"><img src="<?=$meta_values['author_photo_src']?>"></div>
                                                <div class="card-text">
                                                    <h3 class="card-title"><?=$comment->post_title?></h3>
                                                    <p><?=$meta_values['author_post'][0]?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>
                            
                        </div>
                    </div>
                    <div class="reviews-slider__nav">
                        <div class="swiper-prev">
                            <svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 4.75H20.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M1 4.75L4 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M1 4.75L4 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </div>
                        <div class="swiper-next">
                            <svg width="22" height="10" viewBox="0 0 22 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M21 4.75H1.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M21 4.75L18 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M21 4.75L18 8.5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-advantages">
            <div class="container">
                <h3 class="page-title">Why us?</h3>
                <h4 class="page-subtitle">With years of experience in SimCorp Dimension system, business analysis and project management our team provides your business with the following advantages:</h4>
                <div class="advantages-grid__wrap">
                    <div class="advantages-grid">
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-shield ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#shield"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Protect your interests in affairs with original system vendor</div>
                            </div>
                        </div>
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-trophy ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#trophy"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Be faster and more flexible than your competitors, using the Online SimCorp Dimension Support & Development service</div>
                            </div>
                        </div>
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-diamond ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#diamond"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Take an advantage of the high quality expertise service from certified professionals</div>
                            </div>
                        </div>
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-benefit ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#benefit"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Benefit from full control over budgeting & planning, using the “expected development time/costs plan”</div>
                            </div>
                        </div>
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-faster ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#faster"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Gain from SimCorp Dimension knowledge sharing</div>
                            </div>
                        </div>
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-people ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#people"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Get the reliable, fast and effective support for constructed solutions</div>
                            </div>
                        </div>
                        <div class="advantages-col">
                            <div class="advantages-card">
                                <div class="advantages-card__icon">
                                    <svg class="icon icon-enjoy ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#enjoy"></use>
                                    </svg>
                                </div>
                                <div class="advantages-card__text">Enjoy the full control over tasks' status & regular progress updates</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal modal-text mfp-hide" id="modalCard">
        <div class="modal-box"><a class="modal-close js-modalClose" href="javascript:;">
                <svg class="icon icon-close ">
                    <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#close"></use>
                </svg></a>
            <div class="modal-card"></div>
            <div class="modal-card__info">
                <div class="modal-card__info-image js-modalCardImage"></div>
                <div class="modal-card__info-content js-modalCardDescription"></div>
            </div>
        </div>
    </div>


<?php get_footer(); ?>