
<?php get_header(); ?>



         <section id="content"style="margin-bottom: 0px;">
    <!-- Вывод записей (стандартный) #27 (114264) -->
    <div class="section nobg nopadding" style="margin-top: unset; margin-bottom: unset;">
        <div style="background-color: rgba(0, 0, 0, 0); padding-top: 140px; padding-bottom: 60px;">
            <div class="container">
                <div class="text-muted bottommargin-sm">
                    <div class="kama_breadcrumbs">
                    <?php if(function_exists('bcn_display_list')){ bcn_display_list(); } ?>
                    </div>
                </div>
                <div class="heading-block"><h1><?php
			/* translators: Search query. */
			printf( __( 'Результат поиска: %s'), '<span>' . get_search_query() . '</span>' );
			?></h1></div>
                
                <div id="posts" class="small-thumbs">
                <?php
// проверяем есть ли посты в глобальном запросе - переменная $wp_query
if( have_posts() ){
	// перебираем все имеющиеся посты и выводим их
	while( have_posts() ){
		the_post();
		?>

<div class="entry clearfix">
                        <div class="entry-image">
                            <a href="<?php echo get_permalink(); ?>">
                                <img class="image_fade" src="<?php the_post_thumbnail(); ?>" style='opacity: 1;' />
                            </a>
                        </div>
                        <div class="entry-c">
                            <div class="entry-title">
                                <h3><a href="<?php echo get_permalink(); ?>">
                                <?php the_title(); ?> </a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> <?php echo get_the_date('d.m.Y'); ?></li>
                            </ul>
                            <div class="entry-content">
                            <?php add_filter('the_content','htm_image_content_filter',11); ?>
                       
                            <?php echo strip_tags(get_the_content(),'<p>'); ?>
                               
                            </div>
                            <a href="<?php echo get_permalink(); ?>" class="more-link">Читать далее...</a>
                        </div>
                    </div>  
                    
		<?php
	}
	?>

			
						
	<?php
}
// постов нет
else {
	echo "<h2>Не найдено</h2>";
}
?>


</div>

                        
                       

                   
                    
                
                
<?php wp_pagenavi(); ?>
            </div>
        </div>
    </div>
    <!-- / block_114264 -->
</section>
<?php get_footer(); ?>




