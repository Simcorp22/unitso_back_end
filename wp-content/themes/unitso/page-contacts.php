<?php
/*
Template Name: Contacts Page
*/
wp_enqueue_script('unitso-contact-script', get_template_directory_uri() . '/assets/js/contact.js', array('jquery', 'unitso-main-scripts', 'unitso-vendor-scripts'), null, true);

get_header(); 

$page_meta = get_post_meta(get_the_ID(), '', true);
?>
    <div id="modal_form_container">
        <div class="mfp-bg mfp-ready"></div>
        <div class="mfp-wrap mfp-close-btn-in mfp-auto-cursor mfp-ready" tabindex="-1" style="overflow: hidden auto;"><div class="mfp-container mfp-s-ready mfp-inline-holder"><div class="mfp-content"><div class="modal modal-form" id="modalForm">
            <div class="modal-box"><a class="modal-close js-modalClose" href="javascript:;">
                    <svg class="icon icon-close ">
                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#close"></use>
                    </svg></a>
                <div class="modal-head">
                    <h3 class="modal-title">Make an order</h3>
                </div>
            </div>
        <button title="Close (Esc)" type="button" class="mfp-close">×</button></div></div><div class="mfp-preloader">Loading...</div></div>
        </div>
    </div>

    <div id="loading_modal" class="hidden">
        <img src="<?=get_template_directory_uri()?>/assets/images/ajax-loading-icon.jpg" alt="">
    </div>

    <div class="page">
        <section class="page-banner margin_small">
            <div class="container">
                <h1 class="page-banner__title">Contact</h1>
                <div class="page-banner__image image_compas">
                    <picture>
                        <source media="(max-width: 767px)" srcset="<?=get_template_directory_uri()?>/assets/images/compas.png"/>
                        <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/compas.png"/><img src="<?=get_template_directory_uri()?>/assets/images/compas.png"/>
                    </picture>
                </div>
            </div>
        </section>
        <section class="contacts">
            <div class="container">
                <div class="contacts-map">
                    <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo $page_meta['adress'][0] ?>&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
                <div class="contacts-container">
                    <div class="contacts-flex">
                        <div class="contacts-list">
                            <div class="contacts-info">
                                <div class="contacts-info__icon">
                                    <svg class="icon icon-pin ">
                                        <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#pin"></use>
                                    </svg>
                                </div>
                                <div class="contacts-info__title"><?php echo $page_meta['adress'][0] ?></div>
                            </div>
                            <?php
                            if(isset($page_meta['phone_number']))
                            {
                                ?>
                                <div class="contacts-info">
                                    <div class="contacts-info__icon">
                                        <svg class="icon icon-phone ">
                                            <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#phone"></use>
                                        </svg>
                                    </div>
                                    <div class="contacts-info__title"><?php echo $page_meta['phone_number'][0] ?></div>
                                </div>
                                <?php
                            }
                            if(isset($page_meta['email']))
                            {
                                ?>
                                <div class="contacts-info">
                                    <div class="contacts-info__icon">
                                        <svg class="icon icon-info ">
                                            <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#info"></use>
                                        </svg>
                                    </div>
                                    <div class="contacts-info__title"><?php echo $page_meta['email'][0] ?></div>
                                </div>
                                <?php
                            }
                            if(isset($page_meta['work_time']))
                            {
                                ?>
                                <div class="contacts-info">
                                    <div class="contacts-info__icon">
                                        <svg class="icon icon-clock ">
                                            <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#clock"></use>
                                        </svg>
                                    </div>
                                    <div class="contacts-info__title"><?php echo $page_meta['work_time'][0] ?></div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="contacts-text">
                            <p>Chamber of Commerce (KvK) no.: 61629642</p>
                            <p>VAT number (BTW-id): NL002525228B24</p>
                        </div>
                    </div>
                    <div class="contacts-logo__wrap"><a class="contacts-logo" href="https://www.linkedin.com/company/80731384/" target="_blank">
                            <picture>
                                <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=get_template_directory_uri()?>/assets/images/in-2@x2.png"><img src="<?=get_template_directory_uri()?>/assets/images/in-2.png">
                            </picture></a></div>
                </div>
                <form class="contacts-form js-validate">
                    <div class="contacts-form__flex">
                        <div class="contacts-form__left">
                            <div class="form-field">
                                <input class="input" type="text" name="name" placeholder="Your name..." required>
                            </div>
                            <div class="form-field">
                                <input class="input" type="email" name="email" placeholder="Your e-mail..." required>
                            </div>
                            <div class="form-field">
                                <input class="input" type="text" name="phone" placeholder="Your phone number..." minlength="1" required>
                            </div>
                        </div>
                        <div class="contacts-form__right">
                            <div class="form-field">
                                <textarea class="textarea" placeholder="Your message..." name="message" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="contacts-form__submit">
                        <button id="contact_submit_btn" class="button" disabled type="submit"><span class="button__text">Send</span></button>
                    </div>
                </form>
            </div>
        </section>
    </div>

<?php get_footer(); ?>