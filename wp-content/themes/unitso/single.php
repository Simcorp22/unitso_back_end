<?php

wp_enqueue_script('unitso-single-script', get_template_directory_uri() . '/assets/js/single.js', array('jquery', 'unitso-main-scripts', 'unitso-vendor-scripts'), null, true);

get_header();

the_post();

$category = get_the_category();
$first_category = $category[0];
$category_link = get_category_link( $first_category );
?>
<div class="page">
    <div class="page-back">
        <div class="container">
            <div class="back-wrap"><a class="back" href="<?=$category_link?>">
                    <div class="back-icon">
                        <svg class="icon icon-back ">
                            <use xlink:href="<?=get_template_directory_uri()?>/assets/images/sprites.svg#back"></use>
                        </svg>
                    </div>
                    <div class="back-text">Back</div></a></div>
        </div>
    </div>
    <section class="section section-article">
        <div class="container">
            <article class="article">
                <?php
                $post_id = get_the_ID();
                if( has_post_video( get_the_ID() ) )
                {
                    ?>
                        <div class="video-wrap js-videoAboutWrap">
                            <?=get_the_post_video( $post_id, array(400, 224) )?>
                            <!--
                            <div class="video-item js-videoAbout">
                                <iframe src="<?=get_the_post_video_url( $post_id )?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            -->
                        </div>
                    <?php
                }else{
                    $post_thumbnail_url = get_the_post_thumbnail_url( $post_id, array(300, 190) );
                    if($post_thumbnail_url)
                    {
                        ?>
                        <picture>
                            <source media="(max-width: 767px)" srcset="<?=$post_thumbnail_url?>"/>
                            <source media="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)" srcset="<?=$post_thumbnail_url?>"/><img class="single_img js-modalLink" data-mfp-src="#modalImage" src="<?=$post_thumbnail_url?>"/>
                        </picture>
                        <?php
                    }
                }
                ?>
                <h1><?php the_title(); ?></h1>
                <div class="date"><?php the_time('j') ?> of <?php the_time('F') ?> <?php the_time('Y') ?></div>
                <?php the_content(); ?>
            </article>
        </div>
    </section>
</div>

<div class="modal modal-form mfp-hide" id="modalImage">
    <img src="<?=$post_thumbnail_url?>">
</div>

<style>
    .modal{
            background-image: none;
    }
    #modalImage img{
        max-width: 90%;
        max-height: 100%;
    }
    #modalImage{
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100%;
    }
    .single_img{
        cursor: pointer;
    }
</style>



<?php get_footer(); ?>